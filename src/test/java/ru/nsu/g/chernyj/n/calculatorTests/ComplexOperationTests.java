package ru.nsu.g.chernyj.n.calculatorTests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.nsu.g.chernyj.n.calculator.Calculator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class ComplexOperationTests {

    @Test
    public void loopTest(){
        String inputString = "5 dup [ print 1 - dup ]";
        String outputString = "5 4 3 2 1";
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        OutputStream outputStream = new ByteArrayOutputStream();
        Calculator calculator = new Calculator(inputStream, outputStream);
        calculator.calculate();
        String calculatorOutput = outputStream.toString().replaceAll("\r\n", " ").replaceAll("\n", " ");
        outputString += " ";
        Assertions.assertEquals(outputString, calculatorOutput);
    }

    @Test
    public void loopInLoopTest(){
        String inputString = "5\n" +
                "dup [ dup dup [ print 1 - dup ] drop 1 - dup ]";
        String outputString = "5 4 3 2 1 4 3 2 1 3 2 1 2 1 1";
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        OutputStream outputStream = new ByteArrayOutputStream();
        Calculator calculator = new Calculator(inputStream, outputStream);
        calculator.calculate();
        String calculatorOutput = outputStream.toString().replaceAll("\r\n", " ").replaceAll("\n", " ");
        outputString += " ";
        Assertions.assertEquals(outputString, calculatorOutput);
    }

    @Test
    public void defineTest(){
        String inputString = "5\n" +
                "define min\n" +
                "  dup rot dup rot 1 rot rot  <\n" +
                "  [ drop swap drop 0 0 ] \n" +
                "  [ drop 0 ] ;\n" +
                "\n" +
                "7 5 min print";
        String outputString = "5";
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        OutputStream outputStream = new ByteArrayOutputStream();
        Calculator calculator = new Calculator(inputStream, outputStream);
        calculator.calculate();
        String calculatorOutput = outputStream.toString().replaceAll("\r\n", " ").replaceAll("\n", " ");
        outputString += " ";
        Assertions.assertEquals(outputString, calculatorOutput);
    }
}
