package ru.nsu.g.chernyj.n.calculator;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Stack;

public class Calculator {
    private Stack<Integer> stack;
    private Interpreter interpreter;
    private InputStream inputStream;
    private OutputStream outputStream;
    private ArrayList<String> code;
    private OperationFactory operationFactory;
    private LoopManager loopManager;
    private Definer definer;

    public Calculator(InputStream inputStream, OutputStream outputStream) {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
        this.stack = new Stack<Integer>();
        this.interpreter = new Interpreter();
        this.operationFactory = new OperationFactory();
        this.loopManager = new LoopManager();
        this.definer = new Definer();
    }

    public void calculate() {
        code = interpreter.scan(inputStream);
        int command_index = 0;
        for (command_index = 0; command_index < code.size(); command_index++) {
            //Loop
            if (code.get(command_index).equals(Constants.LOOP_START)) {
                command_index = loopManager.loopStart(command_index, stack, code);
            } else if (code.get(command_index).equals(Constants.LOOP_END)) {
                command_index = loopManager.loopEnd(command_index, stack);
            }
            //Define
            else if (code.get(command_index).equals(Constants.OP_DEFINE)) {
                command_index = definer.defineFunction(code, command_index);
            } else try {
                stack.push(Integer.parseInt(code.get(command_index)));
            } catch (NumberFormatException e) {
                if (definer.isDefined(code.get(command_index))) {
                    definer.executeFunction(code, command_index, code.get(command_index));
                    command_index--;
                } else
                    operationFactory.createOperation(code.get(command_index)).execute(stack, outputStream);
            }
        }
    }
}
