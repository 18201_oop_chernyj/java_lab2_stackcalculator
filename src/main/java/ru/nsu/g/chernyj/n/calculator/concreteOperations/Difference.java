package ru.nsu.g.chernyj.n.calculator.concreteOperations;

import org.jetbrains.annotations.NotNull;
import ru.nsu.g.chernyj.n.calculator.Operation;

import java.io.OutputStream;
import java.util.Stack;

public class Difference implements Operation {
    public void execute(@NotNull Stack<Integer> stack, OutputStream outputStream) {
        int a = stack.pop();
        int b = stack.pop();
        stack.push(b - a);
    }
}
