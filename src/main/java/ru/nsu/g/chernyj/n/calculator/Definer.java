package ru.nsu.g.chernyj.n.calculator;

import java.util.ArrayList;
import java.util.HashMap;

public class Definer {
    //TODO: It won't work with recursion
    private HashMap<String, ArrayList<String>> functions;

    public Definer() {
        functions = new HashMap<>();
    }

    public boolean isDefined(String functionName) {
        return functions.containsKey(functionName);
    }

    public void executeFunction(ArrayList<String> code, int command_index, String functionName) {
        if (code.get(command_index) == functionName) {
            code.remove(command_index);
            code.addAll(command_index, functions.get(functionName));
        }
    }

    public int defineFunction(ArrayList<String> code, int command_index) {
        ArrayList<String> function = new ArrayList<String>();
        command_index++;
        String functionName = code.get(command_index);
        command_index++;
        for (; command_index < code.size() && !code.get(command_index).equals(";"); command_index++) {
            function.add(code.get(command_index));
        }
        int continueIndex = command_index;
        functions.put(functionName, function);
        return continueIndex;
    }
}
