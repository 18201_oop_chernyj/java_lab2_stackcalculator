package ru.nsu.g.chernyj.n.calculatorTests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.nsu.g.chernyj.n.calculator.Calculator;

import java.io.*;

public class ConcreteOperationTests {
    @Test
    public void printTest(){
        String inputString = "2 print";
        String outputString = "2";
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        OutputStream outputStream = new ByteArrayOutputStream();
        Calculator calculator = new Calculator(inputStream, outputStream);
        calculator.calculate();
        String calculatorOutput = outputStream.toString().replaceAll("\r\n", " ").replaceAll("\n", " ");
        outputString += " ";
        Assertions.assertEquals(outputString, calculatorOutput);
    }

    @Test
    public void sumTest(){
        String inputString = "2 3 + print";
        String outputString = "5";
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        OutputStream outputStream = new ByteArrayOutputStream();
        Calculator calculator = new Calculator(inputStream, outputStream);
        calculator.calculate();
        String calculatorOutput = outputStream.toString().replaceAll("\r\n", " ").replaceAll("\n", " ");
        outputString += " ";
        Assertions.assertEquals(outputString, calculatorOutput);
    }

    @Test
    public void diffTest(){
        String inputString = "2 3 - print";
        String outputString = "-1";
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        OutputStream outputStream = new ByteArrayOutputStream();
        Calculator calculator = new Calculator(inputStream, outputStream);
        calculator.calculate();
        String calculatorOutput = outputStream.toString().replaceAll("\r\n", " ").replaceAll("\n", " ");
        outputString += " ";
        Assertions.assertEquals(outputString, calculatorOutput);
    }

    @Test
    public void dropTest(){
        String inputString = "2 3 print drop print";
        String outputString = "3 2";
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        OutputStream outputStream = new ByteArrayOutputStream();
        Calculator calculator = new Calculator(inputStream, outputStream);
        calculator.calculate();
        String calculatorOutput = outputStream.toString().replaceAll("\r\n", " ").replaceAll("\n", " ");
        outputString += " ";
        Assertions.assertEquals(outputString, calculatorOutput);
    }

    @Test
    public void duplicateTest(){
        String inputString = "2 dup 1 - print drop print";
        String outputString = "1 2";
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        OutputStream outputStream = new ByteArrayOutputStream();
        Calculator calculator = new Calculator(inputStream, outputStream);
        calculator.calculate();
        String calculatorOutput = outputStream.toString().replaceAll("\r\n", " ").replaceAll("\n", " ");
        outputString += " ";
        Assertions.assertEquals(outputString, calculatorOutput);
    }

    @Test
    public void MultiplyTest(){
        String inputString = "3 2 * print";
        String outputString = "6";
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        OutputStream outputStream = new ByteArrayOutputStream();
        Calculator calculator = new Calculator(inputStream, outputStream);
        calculator.calculate();
        String calculatorOutput = outputStream.toString().replaceAll("\r\n", " ").replaceAll("\n", " ");
        outputString += " ";
        Assertions.assertEquals(outputString, calculatorOutput);
    }

    @Test
    public void RotateTest(){
        String inputString = "1 2 3 rot print drop print drop print";
        String outputString = "1 3 2";
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        OutputStream outputStream = new ByteArrayOutputStream();
        Calculator calculator = new Calculator(inputStream, outputStream);
        calculator.calculate();
        String calculatorOutput = outputStream.toString().replaceAll("\r\n", " ").replaceAll("\n", " ");
        outputString += " ";
        Assertions.assertEquals(outputString, calculatorOutput);
    }

    @Test
    public void SqrtTest(){
        String inputString = "4 sqrt print";
        String outputString = "2";
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        OutputStream outputStream = new ByteArrayOutputStream();
        Calculator calculator = new Calculator(inputStream, outputStream);
        calculator.calculate();
        String calculatorOutput = outputStream.toString().replaceAll("\r\n", " ").replaceAll("\n", " ");
        outputString += " ";
        Assertions.assertEquals(outputString, calculatorOutput);
    }

    @Test
    public void SwapTest(){
        String inputString = "3 2 swap print drop print";
        String outputString = "3 2";
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        OutputStream outputStream = new ByteArrayOutputStream();
        Calculator calculator = new Calculator(inputStream, outputStream);
        calculator.calculate();
        String calculatorOutput = outputStream.toString().replaceAll("\r\n", " ").replaceAll("\n", " ");
        outputString += " ";
        Assertions.assertEquals(outputString, calculatorOutput);
    }

    @Test
    public void CompareLessTest(){
        String inputString = "3 2 < print 2 3 < print";
        String outputString = "0 1";
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        OutputStream outputStream = new ByteArrayOutputStream();
        Calculator calculator = new Calculator(inputStream, outputStream);
        calculator.calculate();
        String calculatorOutput = outputStream.toString().replaceAll("\r\n", " ").replaceAll("\n", " ");
        outputString += " ";
        Assertions.assertEquals(outputString, calculatorOutput);
    }

    @Test
    public void CompareGreaterTest(){
        String inputString = "3 2 > print 2 3 > print";
        String outputString = "1 0";
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        OutputStream outputStream = new ByteArrayOutputStream();
        Calculator calculator = new Calculator(inputStream, outputStream);
        calculator.calculate();
        String calculatorOutput = outputStream.toString().replaceAll("\r\n", " ").replaceAll("\n", " ");
        outputString += " ";
        Assertions.assertEquals(outputString, calculatorOutput);
    }
}
