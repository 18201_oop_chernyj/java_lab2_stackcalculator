package ru.nsu.g.chernyj.n.calculator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

public class OperationFactory {
    private Properties operationProperties;

    public OperationFactory() {
        operationProperties = new Properties();
        getOperations();
    }

    public Operation createOperation(String command) {
        Operation operation = null;
        try {
            String operationClassName = operationProperties.getProperty(command);
            if (operationClassName == null)
                throw new IllegalArgumentException("Command '" + command + "' not found");
            operation = (Operation) Class.forName(operationClassName).getDeclaredConstructor().newInstance();
        } catch (IllegalArgumentException | ClassNotFoundException | NoSuchMethodException |
                InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return operation;
    }

    public Properties getOperations() {
        String opPropFileName = "operations.properties";

        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(opPropFileName);
            if (inputStream != null)
                operationProperties.load(inputStream);
            else
                throw new FileNotFoundException("property file '" + opPropFileName + "' not found");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return operationProperties;
    }


}
