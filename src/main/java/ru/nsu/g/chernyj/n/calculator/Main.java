package ru.nsu.g.chernyj.n.calculator;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator(System.in, System.out);
        calculator.calculate();
    }
}
