package ru.nsu.g.chernyj.n.calculator;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Interpreter {
    private InputStream inputStream;

    public ArrayList<String> scan(InputStream inputStream) {
        ArrayList<String> code = new ArrayList<String>();
        Scanner scanner = new Scanner(inputStream);
        String next;
        while (scanner.hasNext()) {
            next = scanner.next();
            //Skip comments
            if (next.startsWith(Constants.COMMENT))
                scanner.nextLine();
            else
                code.add(next);
        }
        scanner.close();
        return code;
    }

}
