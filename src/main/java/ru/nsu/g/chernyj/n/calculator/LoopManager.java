package ru.nsu.g.chernyj.n.calculator;

import java.util.ArrayList;
import java.util.Stack;

public class LoopManager {

    private Stack<Integer> loopStartIndexes;

    public LoopManager() {
        this.loopStartIndexes = new Stack<Integer>();
    }

    public int loopStart(int commandIndex, Stack<Integer> stack, ArrayList<String> code) {
        if (stack.peek() != 0) {
            loopStartIndexes.push(commandIndex);
        } else {
            int loopCounter = 1;
            for (commandIndex++; commandIndex < code.size() && loopCounter != 0; commandIndex++) {
                if (code.get(commandIndex).equals("["))
                    loopCounter++;
                if (code.get(commandIndex).equals("]"))
                    loopCounter--;
            }
            commandIndex--;
        }
        stack.pop();
        return commandIndex;
    }

    public int loopEnd(int commandIndex, Stack<Integer> stack) {
        if (stack.peek() != 0) {
            stack.pop();
            return loopStartIndexes.peek();
        }
        loopStartIndexes.pop();
        stack.pop();
        return commandIndex;
    }
}