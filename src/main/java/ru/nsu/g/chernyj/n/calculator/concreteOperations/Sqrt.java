package ru.nsu.g.chernyj.n.calculator.concreteOperations;

import org.jetbrains.annotations.NotNull;
import ru.nsu.g.chernyj.n.calculator.Operation;

import java.io.OutputStream;
import java.util.Stack;

public class Sqrt implements Operation {
    public void execute(@NotNull Stack<Integer> stack, OutputStream outputStream) {
        stack.push((int) Math.sqrt(stack.pop()));
    }
}