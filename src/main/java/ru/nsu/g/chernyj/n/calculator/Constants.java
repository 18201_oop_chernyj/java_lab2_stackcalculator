package ru.nsu.g.chernyj.n.calculator;

public final class Constants {
    public static final String COMMENT = "#";
    public static final String LOOP_START = "[";
    public static final String LOOP_END = "]";
    public static final String OP_DEFINE = "define";
    private Constants() {
    }
}
