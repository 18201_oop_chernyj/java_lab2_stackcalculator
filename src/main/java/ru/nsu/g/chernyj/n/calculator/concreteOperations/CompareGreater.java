package ru.nsu.g.chernyj.n.calculator.concreteOperations;

import org.jetbrains.annotations.NotNull;
import ru.nsu.g.chernyj.n.calculator.Operation;

import java.io.OutputStream;
import java.util.Stack;

public class CompareGreater implements Operation {
    public void execute(@NotNull Stack<Integer> stack, OutputStream outputStream) {
        int a = stack.pop();
        int b = stack.pop();
        if (b > a)
            stack.push(1);
        else
            stack.push(0);
    }
}
