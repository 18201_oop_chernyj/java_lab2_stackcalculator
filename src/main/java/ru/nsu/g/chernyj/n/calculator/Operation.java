package ru.nsu.g.chernyj.n.calculator;

import java.io.OutputStream;
import java.util.Stack;

public interface Operation {
    void execute(Stack<Integer> stack, OutputStream outputStream);
}

