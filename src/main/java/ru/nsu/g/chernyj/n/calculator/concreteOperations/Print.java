package ru.nsu.g.chernyj.n.calculator.concreteOperations;

import org.jetbrains.annotations.NotNull;
import ru.nsu.g.chernyj.n.calculator.Operation;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Stack;

public class Print implements Operation {
    public void execute(@NotNull Stack<Integer> stack, OutputStream outputStream) {
        PrintStream writer = new PrintStream(outputStream);
        writer.println(stack.peek());
    }
}
